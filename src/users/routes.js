const express = require('express');
const UserController = require('./controller');
const auth = require('../auth/auth');

const PREFIX = 'api';

const routes = express.Router();

routes.post(`/${PREFIX}/users/register`, UserController.signup);
routes.post(`/${PREFIX}/users/login`, UserController.signin);
routes.get(`/${PREFIX}/users`, auth, UserController.list);
routes.get(`/${PREFIX}/users/:id`, auth, UserController.get);
routes.put(`/${PREFIX}/users/:id`, auth, UserController.edit);

module.exports = routes;