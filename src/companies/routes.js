const express = require('express');
const CompanyController = require('./controller');
const auth = require('../auth/auth');

const PREFIX = 'api';

const routes = express.Router();


routes.get(`/${PREFIX}/companies`, auth, CompanyController.list);
routes.get(`/${PREFIX}/company/:id`, auth, CompanyController.get);
routes.put(`/${PREFIX}/company/:id`, auth, CompanyController.edit);
routes.post(`/${PREFIX}/company/`, auth, CompanyController.create);
routes.delete(`/${PREFIX}/company/:id`, auth, CompanyController.delete);

module.exports = routes;