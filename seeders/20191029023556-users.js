const bcrypt = require('bcrypt');
const saltRounds = 10;

function hashPassword (password) {
  return bcrypt.hashSync(password, saltRounds);
};

module.exports = {
  up: (queryInterface) => {
    return queryInterface.bulkInsert('users', [{
      fullname: 'Lutvi Rosyady',
      username: 'owner',
      email: 'owner@qatros.com',
      password: hashPassword('password'),
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface) => {
    return queryInterface.bulkDelete('users', null, {});
  },
};